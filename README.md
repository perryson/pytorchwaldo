# PytorchWaldo:

I trained a Darknet YOLOv3 model to detect and label Waldo's face. Using the tool [LabelImg](https://github.com/tzutalin/labelImg), I compiled and labeled a dataset of Waldo faces. The program was based on the Medium article [Object detection and tracking in PyTorch](https://towardsdatascience.com/object-detection-and-tracking-in-pytorch-b3cf1a696a98).

# Images
![Waldo Image 1](https://bitbucket.org/perryson/pytorchwaldo/raw/master/images/image1.png)
![Waldo Image 2](https://bitbucket.org/perryson/pytorchwaldo/raw/master/images/image2.png)

#  Steps to display the Jupyter Notebook 
* Select the file Model.ipynb. 
* Select Default File Viewer.
* Select Ipython Notebook.